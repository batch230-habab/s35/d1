const express = require("express");

// Mongoose is a package that allows creationi of Schemas to model our data structures
// Also has access to a number of methods for manipulating databases
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// [SECTION] MongoDB Connection
// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
// Syntax:
    //mongoose.connect("<MongoDB connection string>",{urlNewUrlParser: true})
mongoose.connect("mongodb+srv://admin:admin@batch230.dzsjady.mongodb.net/s35?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
)

// connection to database
//allows to handle errors when the initial connection is established
//works with the on and once mongoose methods
let db = mongoose.connection
//if a connection error occured, output in the console
//console.error.bind(console) allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

app.use(express.json());

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
})

const Task = mongoose.model("Task", taskSchema);

app.post("/tasks",(request, response) =>{
    //check if there are duplicate tasks
    Task.findOne({name: request.body.name}, (err, result) => {
        //If there was found and the document's name matches the information ia the client/Postman
        if(result!=null && result.name == request.body.name){
            return response.send("Duplicate task found");
        }else{
            let newTask = new Task({
                name : request.body.name
            })
            newTask.save((saveErr, savedTask) =>{
                //if an error is saved in saveErr parameter
                if(saveErr){
                    return console.error(saveErr);
                }else{
                    return response.status(201).send("New task created");
                }
            })
        }
    })
})

app.get("/tasks", (request, response) => {
    Task.find({}, (err, result) => {
        if(err){
            return console.log(err);
        }else{
            return response.status(200).json({
                data : result
            })
        }
    })
})
const userSchema = new mongoose.Schema({
    username: String,
    password: String
})
const User = mongoose.model("User",userSchema)

app.post("/signup",(request, response) =>{
    //check if there are duplicate users
    User.findOne({username: request.body.username}, (err, result) => {
        //If there was found and the document's name matches the information ia the client/Postman
        if(result!=null && result.username == request.body.username){
            return response.send("Username has already been taken");
        }else{
            let newUser = new User({
                username : request.body.username,
                password : request.body.password
            })
            newUser.save((saveErr, savedTask) =>{
                //if an error is saved in saveErr parameter
                if(saveErr){
                    return console.error(saveErr);
                }else{
                    return response.status(201).send("New user registered");
                }
            })
        }
    })
})


app.listen(port, () => console.log(`Server running at port ${port}`));